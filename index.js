function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (typeof letter !== 'string' || letter.length !== 1) {
    // If letter is invalid, return undefined.
        return undefined;
    }

    // Convert the sentence to lowercase to make the search case-insensitive.
    const lowerCaseSentence = sentence.toLowerCase();
    
    let count = 0;
    for (let i = 0; i < lowerCaseSentence.length; i++) {
        // Condition 2: Count how many times the letter occurs in the given sentence.
        if (lowerCaseSentence[i] === letter.toLowerCase()) {
            count++;
        }
    }

    return count;
  
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    const lowerCaseText = text.toLowerCase();
    const seenLetters = {};
    // The function should disregard text casing before doing anything else.
    for (let i = 0; i < lowerCaseText.length; i++) {
        const letter = lowerCaseText[i];
    // If the function finds a repeating letter, return false. Otherwise, return true.
        if (seenLetters[letter]) {

            // Condition 3: If a repeating letter is found, return false.
            return false;
        }
        seenLetters[letter] = true;

    }
    return true;
}

function purchase(age, price) {

    if (age < 13) {
        // Return undefined for students aged below 13.
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        // Return discounted price (rounded off) for students aged 13 to 21.
        // Return discounted price (rounded off) for senior citizens.
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2); // Convert to string with 2 decimal places
    } else {
        // Return price (rounded off) for people aged 22 to 64.
        return price.toFixed(2); // Convert to string with 2 decimal places
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    const hotCategories = {};

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    // Loop through each item in the items array.
    for (const item of items) {
        // If the item's stocks are zero, add its category to hotCategories.
        if (item.stocks === 0) {
            hotCategories[item.category] = true;
        }
    }

    // Get the keys (categories) from the hotCategories object and convert them to an array.
     const hotCategoriesArray = Object.keys(hotCategories);

     // Filter out any repeating categories.
     const uniqueHotCategories = [...new Set(hotCategoriesArray)];

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

     return uniqueHotCategories;
}

// Test items
const items = [
    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
];

function findFlyingVoters(candidateA, candidateB) {
    // Create an empty array to store the common voters.
    const flyingVoters = [];

    // Loop through each voter in candidateA.
    for (const voter of candidateA) {
        // Check if the voter also voted for candidate B.
        if (candidateB.includes(voter)) {
            // Add the voter to the flyingVoters array if they voted for both candidates.
            flyingVoters.push(voter);
        }
    }

    return flyingVoters;
    
}

// Test candidates
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};